import { subtract, sum, divide, multiply } from '../src/functions.js';

describe('Divide', () => {
    it('Returns Infinity with parameters 2 and 0', () => {
        const result = divide(2,0);
        expect(result).toBe(Infinity);
    });
});

describe('Multiply', () => {
    it('Returns 24 with parameters 4 and 6', () => {
        const result = multiply(4,6);
        expect(result).toBe(24);
    });
});

describe('Subtract', () => {
    it('Returns 0 with parameters 2 and 2', () => {
        const result = subtract(2,2);
        expect(result).toBe(0);
    });
});

describe('Sum', () => {
    it('Returns 4 with parameters 2 and 2', () => {
        const result = sum(2,2);
        expect(result).toBe(4);
    });
   
    it('Returns 14 with parameters 4 and 10', () => {
        const result = sum(4,10);
        expect(result).toBe(14);
    });
});
 
/*
test('Multiplication test', () => {
    expect(multiply(1,3)).toBe(3);
});
test('Multiplication test', () => {
    expect(multiply(8,3)).toBe(24);
});
test('Multiplication test', () => {
    expect(multiply(9,3)).toBe(27);
});
test('Multiplication test', () => {
    expect(multiply(10,3)).toBe(30);
});
*/